#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio_ext.h>

enum {FALSE,TRUE} bool;
typedef struct {
	int number;
	char name[30];
	char family[30];
	int age;
} social;

//index file struct databases files
typedef struct {
	char databasename[30];
	int count_struct;		
	time_t create_time;
} dbstruct;
/** prototype function - start section ***/ 				  

void ChoiseMenu();				  
int CreateDatabase(char *fname, int countRecord);
int CountRecordDatabase(char *fname);
int AddRecordDatabase(FILE *, int countRecord);
void PrintRecordDatabase(FILE *, int countRecord);
void EraseRecordDatabase(FILE *, int countRecord);

void ExportRecordDatabase(FILE *fp);



/*** prototype function - end section ***/ 				  


int main()
{
	int choise, flag = 1;	
	char *fname = "database.dat";
	FILE *fp = NULL;
	CreateDatabase(fname, 157);
	printf("Count record database %s =  [%10d]\n", fname, CountRecordDatabase(fname));	
	fp = fopen(fname,"r+");
	//AddRecordDatabase(fp,CountRecordDatabase(fname));
	//PrintRecordDatabase(fp, CountRecordDatabase(fname));
	//fclose(fp);
	//return FALSE;
	ChoiseMenu();
	while (flag)
	{
		ChoiseMenu();
		choise = getchar();		
		switch (choise) 
		{			
			case '2':					
					AddRecordDatabase(fp,CountRecordDatabase(fname));
					break;
			case '3':
					EraseRecordDatabase(fp, CountRecordDatabase(fname));
					break;					
			case '5':
					PrintRecordDatabase(fp,CountRecordDatabase(fname));
					break;
			case '6':
		 			ExportRecordDatabase(fp);					
					break;
			case '0':
					fclose(fp);
					return 0;										
			default:
					if (choise == '\n'){
						printf("Wrong choise.Again\n");					
					}
					break;
		}
	}	
	fclose(fp);
	return 0;
}

void PrintRecordDatabase(FILE *fp, int countRecord)
{
	social s;
	int flag = 1, start, end, number;
	char sym;
	char instr[20];
	
	while (flag){
		printf("...................................................................... \n");
		printf(": U can enter number, range, or enter 'all' for view all, exit - exit: \n");
		printf(": Example: number = 1;  range = r1-10; all - all records in database : \n");
		printf("...................................................................... \n");
		printf(" Command: ");
		fgets(instr, sizeof(instr), stdin);
		if (strstr(instr,"exit")){					
			flag = 0; // exit loop
		}
		if (sscanf(instr, "%d", &number)){
			if ((number >= 1) && (number <= countRecord)){			
				fseek(fp,sizeof(social) * (number-1), SEEK_SET);
				if (fread(&s, sizeof(s), 1, fp)){
					printf("Record #[%4d]\n", number);
					printf("\tName: %s\n\tFamily: %s\n\tAge: %d\n", s.name, s.family, s.age);
				} else {
					printf("Error read record #[%4d]\n", number);
				}				 								
			} else {
				printf("Wrong number value. Again.\n");
			}
		}
		if (sscanf(instr, "%[rR]%d-%d", &sym, &start, &end)){
			if ((start >= 1) && (end <= countRecord)){							
				int count = 0;
				fseek(fp,sizeof(social) * (start - 1), SEEK_SET);
				for (count = start; count <= end; count++){																		
					if (fread(&s, sizeof(social), 1, fp)){
						printf("Record #[%4d]\n", count);
						printf("\tName: %s\n\tFamily: %s\n\tAge: %d\n", s.name, s.family, s.age);
					} else {
						printf("Error read record #[%4d]\n", start);
					}				 					
				}												
			} else {
				printf("Not correct range.Again.\n");
			}								
		}
		if (strstr(instr, "all")){
			rewind(fp);
			int count, life = 0;
			for (count = 0; count < countRecord  ; count++){
				
					if (fread(&s, sizeof(social), 1, fp)){
						if (s.number != 0){
							printf("Record #[%4d]\n", (count+1));
							printf("\tName: %s\n\tFamily: %s\n\tAge: %d\n", s.name, s.family, s.age);
							life++;
						}
					} else {
						printf("Error read record #[%4d]\n", start);					
					}				 					
				}
				printf(".........................................\n");
				printf(": Total life records in base: [%4d]    :\n", life);
				printf(":........................................\n");
		}
					
							
	}					
	return;
	
}
void EraseRecordDatabase(FILE *fp, int countRecord)
{
	social s = {0,"","",0};	
	int number;
	printf("Enter number record for erase: ");
	scanf("%d", &number);
	if ( (number >=1) && (number <= countRecord) ){
		fseek(fp, sizeof(s) * (number-1), SEEK_SET);
		if (fwrite(&s, sizeof(s), 1, fp)) {
			printf("Record #[%4d] success delete.\n", number);		
		} else {
			printf("Error erasing record #[%4d]\n", number);
		}
	}			
}
		
					
int AddRecordDatabase(FILE *fp, int countRecord) {
	social s = {0,"", "", 0};
	int number;
	char choise;
	int flag = 1;
	
	while (flag) {							
		printf("Number record [1-%d]: ", countRecord);
		scanf("%d", &number);		
		if ((number >=1) && (number <= countRecord)) {		
			flag = 0; // exit loop			
		} else {
			printf("Wrong range, again\n");
		}					
	}	
	flag = 1;
	
	/* check record empty or no*/	
	fseek(fp, sizeof(social) * (number - 1) , SEEK_SET);
	if (fread(&s, sizeof(s), 1, fp) < 1 ){
		printf("Error reading record number %d.Abort.\n", number);
		return FALSE;
	} else if (s.number != 0) {
		printf("Number = %d\n Name: %s\n", s.number, s.name);		
		while ( flag ){		 
			printf("Record # %d not empty. Rewrite? (y/n):", number);
			scanf("%c", &choise);
			if ((choise == 'y') || (choise =='Y')) {
				flag = FALSE;				
			}
			if ((choise == 'n') || (choise =='N')) {				
				return FALSE;
			}
		}														
	}
	printf("Number = %d  Name: %s\n", s.number, s.name);
	/* add or rewrite record */
	printf("Name: ");
	scanf("%s", s.name);
	printf("Family: ");
	scanf("%s", s.family);
	printf("Age: ");
	scanf("%d", &s.age);
	s.number = number;

	/* write or rewrite record */
	fseek(fp, sizeof(social) * (number - 1) , SEEK_SET);
	if (fwrite(&s, sizeof(s), 1, fp) != 1){
		printf("Error writing record # %d.Abort\n", number);
		return FALSE;
	} 
	printf("Succes writing record # %d.\n", number);
	return TRUE;
}
	
	
		
			
	
void UpdateDatabase(FILE *fp)
{
	if (fp == NULL) 
		return;	
	
	social s;
	
		printf("Social number:" ); // key structure 1 = 1 structure etc
		scanf("%d",&s.number);
		printf("Name:");
		scanf("%s",s.name);
		printf("Family:");
		scanf("%s",s.family);
		printf("Age:");
		scanf("%d",&s.age);
		if ( (fwrite(&s, sizeof(social) * s.number, 1, fp) ) != 1)
		{
			printf("Error write struct №%04d.Abort.\n", s.number);			
			return;
		}
		printf("Update info for №%04d success.\n",  s.number);		
		return;
}		



void ExportRecordDatabase(FILE *fp)
{
//	FILE *fpout;
//	char *txtfile = "some.txt";
	social s;	
	
	while(!feof(fp)){
		fread(&s, sizeof(s), 1, fp);
		printf("Number: %4d %10s  %10s %2d\n", s.number, s.name, s.family, s.age);
	}	
	
/*		
		if ((fpout = fopen(txtfile, "w")) == NULL )
		{ 
			printf("Error opening %s. Abort.\n", txtfile);			
			return;
		}
		
		fprintf(fpout,"%10s%10s%15s%4s\n","Number", "Name","Family","Age");		
		while (!feof(fp))
		{
			fread(&s, sizeof(s), 1, fp);		
			fprintf(fpout, "%10d%10s%15s%4d\n",s.number, s.name, s.family, s.age);
//			printf("%10d%10s%15s%4d\n",s.social_number, s.name, s.family, s.age);
		}	
	fclose(fpout);	*/
	return;
}		


// создание файла индекса баз данных
int CreateIndexDatabases(char *fname){
	FILE *fp;
	
}


int CreateDatabase(char *fname, int countRecord)
{
	FILE *fp;
	social s_array[countRecord];
	int index = 0;
	
	/* clear array */
	for (index = 0; index < countRecord; index++){
		s_array[index].number = 0;
		strcpy(s_array[index].name, "");
		strcpy(s_array[index].family, "");
		s_array[index].age = 0;
	}
	/* check exists database file */									
	if ((fp= fopen(fname, "r")) != NULL) 
	{
		char sym;
		printf("File %s already exists. Overwrite?(y/n):", fname);
		__fpurge(stdin);
		scanf("%c", &sym);		
		
		if (( sym == 'Y') || (sym == 'y')){			
			printf("Choise overwrite...\n");															
		} else  if ((sym == 'N') || (sym == 'n')){
			printf("Choise no. Abort. Choise other name dbname and run again.\n");		
			return FALSE;			
		}
	}
															
	if( (fp=fopen(fname, "w")) == NULL )
	{
		printf("Error open %s file.Abort.\n", fname);
		return FALSE;
	} else 	if ((fwrite(&s_array, sizeof(s_array), 1, fp)) < 1 ) {				
					printf("error writing empty structs.abort");
					fclose(fp);					
					return FALSE;
				}						
	printf("Success creating database '%s'\n", fname);			
	fclose(fp);
	return TRUE;								
}


int CountRecordDatabase(char *fname){
	FILE *fp;
	int count = 0;
	social s;	
	
	if ((fp=fopen(fname, "r")) == NULL ){
		printf("Error open %s file.Abort.\n", fname);
		return FALSE;
	}
	while(!feof(fp)){
		fread(&s, sizeof(s), 1, fp);		
		count++;
	}	
	fclose(fp);
	return count - 1;
}

void ChoiseMenu()
{		
	printf(" ................................................. \n");
	printf(" :Simple programm for working structure and files: \n");
	printf(" ................................................. \n");
	printf(" :                     					        : \n"); 
	printf(" : 2. Add record to database                     : \n");
	printf(" : 3. Erase record in database                   : \n");	
	printf(" : 5. Print record database                      : \n");
	printf(" : 6. Export database                            : \n");
	printf(" : 0. Exit                                       : \n");
	printf(" :...............................................: \n");
	printf("   Your choise: ");		
}
	
